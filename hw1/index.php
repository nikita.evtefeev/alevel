<?php


    $name = 'Nikita';

    echo "<h1>$name</h1><br>";

    $age = 23;

    echo "<h2>$age</h2><br>";

    $pi = 3.14159265359;


    echo $pi.'<br>';

    $m1 = ['alex', 'vova', 'tolya'];


    echo '<pre>';

    print_r($m1);

    echo '</pre>';

    $m2 = ['alex', 'vova', 'tolya', ['kostya', 'olya']] ;

    echo '<pre>';
    print_r($m2);
    echo '</pre>';


    $m3 = ['alex', 'vova', 'tolya', ['kostya', 'olya', ['gosha', 'mila']]];

    echo '<pre>';
    print_r($m3);
    echo '</pre>';

    $m4 = [['alex', 'vova', 'tolya'], ['kostya', 'olya'], ['gosha', 'mila']];

    echo '<pre>';
    print_r($m4);
    echo '</pre>';

