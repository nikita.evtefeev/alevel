<?php


//@ author Nikita Evtefeev <nikita.evtefeev@gmail.com>


//Exercise 1

$a = 42;
$b = 55;

$big = $a > $b ? $a : $b;

echo $big;

echo '<br><br>';

//Exercise 2

$a = rand(5, 15);
$b = rand(5, 15);

echo $a . ' ' . $b;

echo '<br>';

$big = $a > $b ? $a : $b;

echo $big;

echo '<br><br>';

//Exercise 3

$firstname = 'Nikita';
$lastname = 'Evtefeev';
$surname = 'Maksimovich';

$out = $lastname . ' ' . $firstname[0] . '. ' . $surname[0] . '.';

echo $out;

echo '<br><br>';

//Exercise 4

$number = rand(1, 99999);
$num = 7;

echo 'In ' . $number . ' ' . substr_count($number, $num) . ' "' . $num . '"';


echo '<br><br>';

//Exercise 5

$a = 3;

echo $a;
echo '<br>';

$a = 10;
$b = 2;

echo $a + $b;
echo '<br>';

echo $a - $b;
echo '<br>';

echo $a * $b;
echo '<br>';

echo $a / $b;
echo '<br>';

echo '<br>';

$c = 15;
$d = 2;

$result = $c + $d;

echo $result;
echo '<br>';

$a = 10;
$b = 2;
$c = 5;

echo $a + $b + $c;

echo '<br>';

$a = 17;
$b = 10;

$c = $a - $b;
$d = 21;

$result = $c + $d;
echo $result;


echo '<br><br>';

//Exercise 7

$text = 'Привет, Мир!';
echo $text;

echo '<br>';

$text1 = 'Привет, ';
$text2 = 'Мир!';

echo $text1 . $text2;

echo '<br>';
echo '<br>';


$h = 60 * 60;
$d = $h * 24;
$w = $d * 7;
$m = $d * 30;

echo 'In hour ' . $h . ' seconds<br>';
echo 'In day ' . $d . ' seconds<br>';
echo 'In week ' . $w . ' seconds<br>';
echo 'In month ' . $m . ' seconds<br>';

echo '<br><br>';

//Exercise 8

$var = 1;
$var += 12;
$var -= 14;
$var *= 5;
$var /= 7;
$var %= 1;
echo $var;
echo '<br><br>';

//Level **

//Exercise 1
$h = 12;
$m = 34;
$s = 45;

echo "$h:$m:$s";
echo '<br><br>';
//Exercise 2

$text = 'Я';
$text .= ' хочу';
$text .= ' знать';
$text .= ' PHP!';
echo $text;

echo '<br><br>';
//Exercise 3

$foo = 'bar';

$bar = 10;

echo $$foo;

echo '<br><br>';

//Exercise 4

$a = 2;
$b = 4;
echo $a++ + $b; //6
echo $a + ++$b; //8
echo ++$a + $b++; //9

echo '<br><br>';

//Exercise 5

$a = 'asd';
echo isset($a) ? 'переменная существует' : 'переменная не существует';
echo '<br>';

echo 'Переменная имеет тип ' . gettype($a);

echo '<br>';

echo is_null($a) ? 'в переменной нет значения' : 'переменная имеет определенное значение';
echo '<br>';

echo empty($a) ? 'переменная пустая' : 'пееменная не пустая';
echo '<br>';

echo is_integer($a) ? 'переменная целое число' : 'переменная не целое число';
echo '<br>';

echo is_double($a) ? 'переменная дробное число' : 'переменная не дробное число';
echo '<br>';

echo is_string($a) ? 'переменная строка' : 'переменная не строка';
echo '<br>';

echo is_numeric($a) ? 'переменная цисловая' : 'переменная не числовая';
echo '<br>';


echo is_bool($a) ? 'переменная логическая' : 'переменная не логическая';
echo '<br>';

echo is_scalar($a) ? 'переменная скалярная' : 'переменная не скалярная';
echo '<br>';

echo is_array($a) ? 'переменная это массив' : 'переменная это не массив';
echo '<br>';

echo is_object($a) ? 'переменная это объект' : 'переменная это не объект';
echo '<br>';

echo '<br>';
echo '<br>';
//Level ***

//Exercise 1

$a = 5;
$b = 7;

echo $a + $b;
echo '<br>';
echo $a * $b;
echo '<br>';

//Exercise 2
$a = 8;
$b = 9;

echo $a ** 2 + $b ** 2;
echo '<br>';

//Exercise 3
$a = 9;
$b = 6;
$c = 5;

echo ($a + $b + $c) / 3;
echo '<br>';

//Exercise 4
$x = 45;
$y = 67;
$z = 98;

echo ($x + 1) - 2 * ($z - 2 * $x + $y);
echo '<br>';

//Exercise 5

$num = 45532;

echo $num % 3;
echo '<br>';
echo $num % 5;
echo '<br>';

$n = 23;

$n += 0.3 * $n;

echo $n;
echo '<br>';
$n += 1.2 * $n;

echo $n;

echo '<br>';

//Exercise 6
$a = 56;
$b = 76;
echo 0.4 * $a + 0.84 * $b;
echo '<br>';

$num = 343;

$s = (string)$num;
echo (integer)$s[0] + (integer)$s[1] + (integer)$s[2];


echo '<br>';

//Exercise 7

$num = 935;

$s = (string)$num;

$s[1] = 0;

$num = (integer)$s;

echo $num;

echo '<br>';

$num2 = (integer)($s[2] . $s[1] . $s[0]);

echo $num2;

echo '<br>';

//Exercise 8

$a = 34;

echo $a.' ';
echo $a % 2 == 0 ? 'Четное' : 'Не четное';